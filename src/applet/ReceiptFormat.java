package applet;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.Hashtable;

class ReceiptFormat
  implements Printable
{
  private static final String FONT_NAME = "TH SarabunPSK";
  private static final int QUEUE_FONT_SIZE = 60;
  private static final int HEAD_FONT_SIZE = 18;
  private static final int CHANNEL_FONT_SIZE = 16;
  private static final int WAITING_Q_FONT_SIZE = 16;
  private static final int DATE_FONT_SIZE = 12;
  private static final int LANDOFFICE_FONT_SIZE = 12;
  private static final int HEAD_X = 40;
  private static final int HEAD_Y = 15;
  private float QUEUE_X = 65.0F;
  private static final float QUEUE_Y = 55.0F;
  private static final int CHANNEL_X = 60;
  private static final int CHANNEL_Y = 72;
  private static final int WAITING_Q_X = 68;
  private static final int WAITING_Q_Y = 88;
  private static final int QR_X = 58;
  private static final int QR_Y = 80;
  private float DATE_X = 35.0F;
  private float TEXT_X = 35.0F;
  private static final float DATE_Y = 180.0F;
  private float LANDOFFICE_X = 50.0F;
  private static final float LANDOFFICE_Y = 195.0F;
  private static final String HEADER_STRING = "กรมที่ดิน กระทรวงมหาดไทย";
  private static final String WAITING_PREFIX_STRING = "จำนวนคิวรอ : ";
  private static final String WAITING_SUBFIX_STRING = " คิว";
  private static final String DATE_PRIFIX_STRING = "วันที่ ";
  private static final String TIME_PREFIX_STRING = "เวลา ";
  private static final String TIME_SUBFIX_STRING = " น. ";
  private static final int LINE_Y = 205;
  private static final int QR_SIZE = 100;
  private String queueNo = "";
  private String channelNo = "";
  private String qrText = "";
  private String landofficeName = "";
  private String date = "";
  private String time = "";
  private String waitingQueueCount = "";

  public ReceiptFormat(String queueNo, String channelNo, String qrText, String landofficeName, String date, String time, String waitingQueueCount) { this.queueNo = queueNo;
    this.channelNo = channelNo;
    this.qrText = qrText;
    this.landofficeName = landofficeName;
    this.date = date;
    this.time = time;
    this.waitingQueueCount = waitingQueueCount;
    calculatePosition(); }

  private void calculatePosition()
  {
    AffineTransform transform = new AffineTransform();
    FontRenderContext render = new FontRenderContext(transform, true, true);

    Font queueFont = new Font("TH SarabunPSK", 0, 60);
    double queueWidth = queueFont.getStringBounds(this.queueNo, render).getWidth();
    this.QUEUE_X = (float)(110.0D - queueWidth / 2.0D);

    Font landofficeFont = new Font("TH SarabunPSK", 0, 12);
    double landofficeWidth = landofficeFont.getStringBounds(this.landofficeName, render).getWidth();
    this.LANDOFFICE_X = (float)(110.0D - landofficeWidth / 2.0D);

    String dateStr = "เวลา " + this.time + " น. " + "วันที่ " + this.date;
    Font dateFont = new Font("TH SarabunPSK", 0, 12);
    double dateWidth = dateFont.getStringBounds(dateStr, render).getWidth();
    this.DATE_X = (float)(110.0D - dateWidth / 2.0D);
    
    String textStr = "สามารถตรวจสอบสถานะคิวปัจจุบันจาก QR CODE นี้";
    Font textFont = new Font("TH SarabunPSK", 0, 10);
    double textWidth = textFont.getStringBounds(textStr, render).getWidth();
    this.TEXT_X = (float)(110.0D - textWidth / 2.0D);
  }

  public int print(Graphics graphic, PageFormat pageFormat, int pageIndex)
    throws PrinterException
  {
    Graphics2D graphics = (Graphics2D)graphic;

    graphics.setFont(new Font("TH SarabunPSK", 0, 18));
    graphics.drawString("กรมที่ดิน กระทรวงมหาดไทย", 40, 15);
    graphics.setFont(new Font("TH SarabunPSK", 0, 60));
    graphics.drawString(this.queueNo, this.QUEUE_X, 55.0F);
    graphics.setFont(new Font("TH SarabunPSK", 0, 16));
    graphics.drawString("หมายเลขช่องบริการ : " + this.channelNo, 60, 72);

    if (!this.qrText.equals("")) {
      Hashtable hintMap = new Hashtable();
      hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
      QRCodeWriter qrCodeWriter = new QRCodeWriter();

      int matrixWidth = 0;
      try {
        BitMatrix byteMatrix = qrCodeWriter.encode(this.qrText, 
          BarcodeFormat.QR_CODE, 100, 100, hintMap);
        matrixWidth = byteMatrix.getWidth();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(58, 80, matrixWidth + 58, matrixWidth + 80);

        graphics.setColor(Color.BLACK);

        for (int i = 58; i < matrixWidth + 58; i++) {
          for (int j = 80; j < matrixWidth + 80; j++) {
            if (byteMatrix.get(i - 58, j - 80))
              graphics.fillRect(i, j, 1, 1);
          }
        }
      }
      catch (WriterException e)
      {
        e.printStackTrace();
      }
      graphics.setFont(new Font("TH SarabunPSK", 0, 16));
      graphics.drawString("จำนวนคิวรอ : " + this.waitingQueueCount + " คิว", 68, 88);
      graphics.setFont(new Font("TH SarabunPSK", 0, 10));
      graphics.drawString("สามารถตรวจสอบสถานะคิวปัจจุบันจาก QR CODE นี้", this.TEXT_X, 173.0F);
      graphics.setFont(new Font("TH SarabunPSK", 0, 12));
      graphics.drawString("เวลา " + this.time + " น. " + "วันที่ " + this.date, this.DATE_X, 185.0F);
      graphics.setFont(new Font("TH SarabunPSK", 0, 12));
      graphics.drawString(this.landofficeName, this.LANDOFFICE_X, 200.0F);
      graphics.setColor(Color.BLACK);
      graphics.drawLine(0, 210, 225, 210);
    }
    return 0;
  }
}