package applet;

import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import javax.print.PrintService;

class RegsReceiptApplet
{
  public static final int PAPER_WIDTH = 220;
  private static int PAPER_HEIGHT = 400;
  private static int MARGIN_WIDTH = 0;
  private static int MARGIN_HEIGHT = 0;
  private static int PAPER_WIDTH_WITHOUT_MARGIN = 220 - MARGIN_WIDTH - MARGIN_WIDTH;
  private static int PAPER_HEIGHT_WITHOUT_MARGIN = PAPER_HEIGHT - MARGIN_HEIGHT - MARGIN_HEIGHT;

  private static final String[] NOT_IN_SERVICE = { "LQ" };

  public void action(String queueNo, String channelNo, String qrText, String landofficeName, String date, String time, String waitingQueueCount) { PrinterJob job = PrinterJob.getPrinterJob();
    try {
      if (checkService(job.getPrintService())) {
        Book book = new Book();
        book.append(new ReceiptFormat(queueNo, channelNo, qrText, landofficeName, date, time, waitingQueueCount), getPageFormat());
        job.setPageable(book);
        job.print();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private boolean checkService(PrintService service)
    throws Exception
  {
    for (String serviceName : NOT_IN_SERVICE) {
      if (service.getName().contains(serviceName))
        return false;
    }
    return true;
  }
  public static PageFormat getPageFormat() {
    Paper paper = new Paper();
    paper.setSize(220.0D, PAPER_HEIGHT);
    paper.setImageableArea(MARGIN_WIDTH, MARGIN_HEIGHT, PAPER_WIDTH_WITHOUT_MARGIN, PAPER_HEIGHT_WITHOUT_MARGIN);
    PageFormat pageFormat = new PageFormat();
    pageFormat.setOrientation(1);
    pageFormat.setPaper(paper);

    return pageFormat;
  }
}