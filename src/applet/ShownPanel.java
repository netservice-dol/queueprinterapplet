package applet;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Hashtable;
import javax.swing.JPanel;

class ShownPanel extends JPanel
{
  private String qrCodeText;
  private int size = 125;

  public void generateQRCode(String codeText) {
    this.qrCodeText = codeText;
    repaint();
  }

  public void paint(Graphics g)
  {
    super.paint(g);
    if (!this.qrCodeText.equals("")) {
      Hashtable hintMap = new Hashtable();
      hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
      QRCodeWriter qrCodeWriter = new QRCodeWriter();
      try
      {
        BitMatrix byteMatrix = qrCodeWriter.encode(this.qrCodeText, 
          BarcodeFormat.QR_CODE, this.size, this.size, hintMap);
        int matrixWidth = byteMatrix.getWidth();

        Graphics2D graphics = (Graphics2D)g;
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, matrixWidth, matrixWidth);
        graphics.setColor(Color.BLACK);

        for (int i = 0; i < matrixWidth; i++) {
          for (int j = 0; j < matrixWidth; j++) {
            if (byteMatrix.get(i, j))
              graphics.fillRect(i, j, 1, 1);
          }
        }
      }
      catch (WriterException e)
      {
        e.printStackTrace();
      }
    }
  }
}