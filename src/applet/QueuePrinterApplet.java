package applet;

import java.applet.Applet;

public class QueuePrinterApplet extends Applet
{
  private static final long serialVersionUID = 1619275441117451434L;

  public synchronized String printQueue(String queueNo, String channel, String qrText, String landofficeName, String date, String time, String waitingQueueCount)
  {
    RegsReceiptApplet rpt = new RegsReceiptApplet();
    try {
      rpt.action(queueNo, channel, qrText, landofficeName, date, time, waitingQueueCount);
    } catch (Exception e) {
      e.printStackTrace();
      return "2";
    }
    return "1";
  }

  public static void main(String[] args) {
    RegsReceiptApplet applet = new RegsReceiptApplet();
    try {
      applet.action("B001", "2", "http://110.164.49.215:9080/QueueOnlineServer/ubon15.jsp", "สำนักงานที่ดินจังหวัดสงขลา สาขาหาดใหญ่ ส่วนแยกบางกล่ำ", "17 กันยายน 2558", "14:53", "5");
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}