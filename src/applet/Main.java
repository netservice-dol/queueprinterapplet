package applet;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Hashtable;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Main extends JFrame
{
  private JTextField textField;
  private ShownPanel shownPanel;

  public Main()
  {
    this.textField = new JTextField();
    JButton button = new JButton("Generate");
    this.shownPanel = new ShownPanel();
    JPanel panel = new JPanel(new GridLayout(1, 2));
    panel.add(this.textField);
    panel.add(button);
    button.addActionListener((ActionListener) new Main()); //button.addActionListener(new Main.1(this));

    setLayout(new GridLayout(2, 1));
    add(this.shownPanel);
    add(panel);
    setDefaultCloseOperation(3);
    setVisible(true);
    setSize(400, 500);
  }

  public static void main(String[] args)
  {
    String qrCodeText = "http://110.164.49.215:9080/QueueOnlineServer/uat.jsp";
    String filePath = "./QRUAT.png";
    int size = 125;
    String fileType = "png";
    File qrFile = new File(filePath);
    try {
      createQRImage(qrFile, qrCodeText, size, fileType);
    }
    catch (WriterException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("DONE");
  }

  private static void createQRImage(File qrFile, String qrCodeText, int size, String fileType)
    throws WriterException, IOException
  {
    Hashtable hintMap = new Hashtable();
    hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    QRCodeWriter qrCodeWriter = new QRCodeWriter();
    BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText, 
      BarcodeFormat.QR_CODE, size, size, hintMap);

    int matrixWidth = byteMatrix.getWidth();
    BufferedImage image = new BufferedImage(matrixWidth, matrixWidth, 
      1);
    image.createGraphics();

    Graphics2D graphics = (Graphics2D)image.getGraphics();
    graphics.setColor(Color.WHITE);
    graphics.fillRect(0, 0, matrixWidth, matrixWidth);

    graphics.setColor(Color.RED);

    for (int i = 0; i < matrixWidth; i++) {
      for (int j = 0; j < matrixWidth; j++) {
        if (byteMatrix.get(i, j)) {
          graphics.fillRect(i, j, 1, 1);
        }
      }
    }
    ImageIO.write(image, fileType, qrFile);
  }
}